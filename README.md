# result-analyser

Project use Apache Common-Math3 for statistics collecting: 

```
<dependency>
    <groupId>org.apache.commons</groupId>
    <artifactId>commons-math3</artifactId>
    <version>3.6.1</version>
</dependency>
```

## Getting started

### Installing 

```
mvn clean install
```

### Example of usage

Create a `Analyser` instance via builder. All parameters are optional however to get some data you need to define at least `inputFiles` and one `columnMapper`
```
Analyser resultsAnalyser = Analyser.newBuilder()
        .inputFiles("C:\\pathtofile\\file1", "C:\\pathtofile\\file2", "C:\\pathtofile\\file3")
        .dataFormatter(new DoNothingDataFormatter())
        .splitter(new SimpleSplitter(";"))
        .addColumnMapper(new NumberColumnMapper(3, "Column1"))
        .addColumnMapper(new NumberColumnMapper(4, "Column2"))
        .build();
```

Perform analyse. `DescriptiveStatistics` and `Frequency` objects are created (from Apache Math3 lib) based on your input data. 
```
List<ComponentStatistics> componentStatisticsList = resultsAnalyser.analyse();
```
Create `ReportElements` object. You can define here what stats you are interested in by enabling or disabling them in builder.
By default all values are enabled and distribution range and percentiles steps are defined like this:
```
ReportElements reportElements = ReportElements.newBuilder()
        .setDistributionRange(0.1, 1.5, 0.1) //these are defaults values - im setting them just for showcase purpose
        .setPercentilesValues(10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0, 95.0, 99.0, 99.9) //these are defaults values - im setting them just for showcase purpose
        .build();
```
Create `ReportNumberFormat` to define what number format you expect in final report
```
ReportNumberFormat reportNumberFormat = new ReportNumberFormat("#.#", ',', 6);
```
Define output report printer and call prints for all `ComponentStatistics`. Below you can see a CSV-like format console printer example.
You can also save it to the file by creating new instance of `FileComponentStatisticsPrinter`
```
ComponentStatisticsPrinter printer = new CSVFormatConsoleComponentStatisticsPrinter(reportElements, reportNumberFormat, ';');
componentStatisticsList.forEach(printer::print);
```
Sample output (`CSVFormatConsoleComponentStatisticsPrinter`):
```
Column1
---------------------------
Count;1312087
Mean;0,026353
Stdev;0,140326
Min;0,006
Max;27,148
Percentiles
10,0;0,008
20,0;0,009
30,0;0,01
40,0;0,012
50,0;0,013
60,0;0,016
70,0;0,021
80,0;0,028
90,0;0,038
95,0;0,053
99,0;0,233
99,9;1,038
Percentage Distribution
0 - 0,1;0,974992
0,1 - 0,2;0,013428
0,2 - 0,3;0,004362
0,3 - 0,4;0,002436
0,4 - 0,5;0,001295
0,5 - 0,6;0,000774
0,6 - 0,7;0,000585
0,7 - 0,8;0,000447
0,8 - 0,9;0,000344
0,9 - 1;0,000256
1 - 1,1;0,000191
1,1 - 1,2;0,000132
1,2 - 1,3;0,000109
1,3 - 1,4;0,000094
1,4 - 1,5;0,000085
1,5 - ;0,00047

Column2
---------------------------
Count;1312087
Mean;0,026353
Stdev;0,140326
Min;0,006
Max;27,148
Percentiles
10,0;0,008
20,0;0,009
30,0;0,01
40,0;0,012
50,0;0,013
60,0;0,016
70,0;0,021
80,0;0,028
90,0;0,038
95,0;0,053
99,0;0,233
99,9;1,038
Percentage Distribution
0 - 0,1;0,974992
0,1 - 0,2;0,013428
0,2 - 0,3;0,004362
0,3 - 0,4;0,002436
0,4 - 0,5;0,001295
0,5 - 0,6;0,000774
0,6 - 0,7;0,000585
0,7 - 0,8;0,000447
0,8 - 0,9;0,000344
0,9 - 1;0,000256
1 - 1,1;0,000191
1,1 - 1,2;0,000132
1,2 - 1,3;0,000109
1,3 - 1,4;0,000094
1,4 - 1,5;0,000085
1,5 - ;0,00047
```