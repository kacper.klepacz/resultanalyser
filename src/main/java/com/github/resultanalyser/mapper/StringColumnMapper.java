package com.github.resultanalyser.mapper;

import java.util.List;

public class StringColumnMapper extends AbstractColumnMapper<String> {

    protected StringColumnMapper(int columnIndex, String columnName) {
        super(columnIndex, columnName);
    }

    @Override
    public String map(List<String> columns) {
        return columns.get(columnIndex);
    }
}
