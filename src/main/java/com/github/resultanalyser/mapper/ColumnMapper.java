package com.github.resultanalyser.mapper;

import java.util.List;

public interface ColumnMapper<T> {
    T map(List<String> columns) throws MappingException;

    String getColumnName();
}
