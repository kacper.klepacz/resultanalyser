package com.github.resultanalyser.formatter;

public interface DataFormatter {
    String format(String data);
}
