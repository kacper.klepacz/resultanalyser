package com.github.resultanalyser.formatter;

public class DoNothingDataFormatter implements DataFormatter {
    @Override
    public String format(String data) {
        return data;
    }
}
