package com.github.resultanalyser;

import com.github.resultanalyser.formatter.DataFormatter;
import com.github.resultanalyser.formatter.DoNothingDataFormatter;
import com.github.resultanalyser.mapper.ColumnMapper;
import com.github.resultanalyser.mapper.MappingException;
import com.github.resultanalyser.splitter.SimpleSplitter;
import com.github.resultanalyser.splitter.Splitter;
import org.apache.commons.math3.stat.Frequency;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Analyser {
    private static final Logger log = LoggerFactory.getLogger(Analyser.class);
    private final List<String> fileNames;
    private final List<ColumnMapper> columnMappers;
    private final Splitter splitter;
    private final DataFormatter dataFormatter;
    private final Map<ColumnMapper, Frequency> frequencyMap = new HashMap<>();
    private final Map<ColumnMapper, DescriptiveStatistics> descriptiveStatisticsMap = new HashMap<>();

    private Analyser(List<String> fileNames, List<ColumnMapper> columnMappers, Splitter splitter, DataFormatter dataFormatter) {
        this.fileNames = fileNames;
        this.columnMappers = columnMappers;
        this.splitter = splitter;
        this.dataFormatter = dataFormatter;
        init();
    }

    private void init() {
        columnMappers.forEach(columnMapper -> {
            frequencyMap.put(columnMapper, new Frequency());
            DescriptiveStatistics descriptiveStatistics = new DescriptiveStatistics();
            descriptiveStatistics.setPercentileImpl(new ExcelPercentile());
            descriptiveStatisticsMap.put(columnMapper, descriptiveStatistics);
        });
    }

    public List<ComponentStatistics> analyse() {
        Stream<String> dataStream = FileReader.getContentStream(fileNames);

        dataStream.map(dataFormatter::format)
                .map(splitter::split)
                .forEach(this::collectDataFromSplitLine);

        return columnMappers.stream()
                .map(columnMapper -> new ComponentStatistics(columnMapper.getColumnName(), descriptiveStatisticsMap.get(columnMapper), frequencyMap.get(columnMapper)))
                .collect(Collectors.toList());
    }


    private void collectDataFromSplitLine(List<String> columns) {
        columnMappers.forEach(columnMapper -> {
            try {
                double mapped = (Double) columnMapper.map(columns);
                descriptiveStatisticsMap.get(columnMapper).addValue(mapped);
                frequencyMap.get(columnMapper).addValue(mapped);
            } catch (MappingException e) {
                log.error("Skipping value because of following exception: " + e.getMessage());
            }
        });
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {
        private List<String> fileNames = new ArrayList<>();
        private List<ColumnMapper> columnMappers = new ArrayList<>();
        private Splitter splitter = new SimpleSplitter(";");
        private DataFormatter dataFormatter = new DoNothingDataFormatter();

        public Builder inputFiles(String... fileNames) {
            this.fileNames = Arrays.asList(fileNames);
            return this;
        }

        public Builder addColumnMapper(ColumnMapper columnMapper) {
            this.columnMappers.add(columnMapper);
            return this;
        }

        public Builder splitter(Splitter splitter) {
            this.splitter = splitter;
            return this;
        }

        public Builder dataFormatter(DataFormatter dataFormatter) {
            this.dataFormatter = dataFormatter;
            return this;
        }

        public Analyser build() {
            return new Analyser(fileNames, columnMappers, splitter, dataFormatter);
        }
    }

}
