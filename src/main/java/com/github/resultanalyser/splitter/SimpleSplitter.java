package com.github.resultanalyser.splitter;

import java.util.Arrays;
import java.util.List;

public class SimpleSplitter implements Splitter {
    private final String separator;

    public SimpleSplitter(String separator) {
        this.separator = separator;
    }

    @Override
    public List<String> split(String input) {
        return Arrays.asList(input.split(separator));
    }
}
