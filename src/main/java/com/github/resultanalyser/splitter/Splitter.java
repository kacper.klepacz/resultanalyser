package com.github.resultanalyser.splitter;

import java.util.List;

public interface Splitter {
    List<String> split(String input);
}
