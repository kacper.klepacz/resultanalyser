package com.github.resultanalyser.printer;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class ReportNumberFormat {
    private final DecimalFormat df;

    public ReportNumberFormat(String format, char separator, int maxFractionDigits) {
        df = new DecimalFormat(format);
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator(separator);
        df.setMaximumFractionDigits(maxFractionDigits);
        df.setDecimalFormatSymbols(dfs);
    }

    public DecimalFormat getDf() {
        return df;
    }
}
