package com.github.resultanalyser.printer;

import com.github.resultanalyser.ComponentStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CSVFormatConsoleComponentStatisticsPrinter extends BaseComponentStatisticsPrinter {
    private static final Logger log = LoggerFactory.getLogger(CSVFormatConsoleComponentStatisticsPrinter.class);
    private final char columnSeparator;

    public CSVFormatConsoleComponentStatisticsPrinter(ReportElements reportElements, ReportNumberFormat reportNumberFormat, char columnSeparator) {
        super(reportElements, reportNumberFormat);
        this.columnSeparator = columnSeparator;
    }

    @Override
    public void print(ComponentStatistics componentStatistics) {
        StringBuilder sb = new StringBuilder(System.lineSeparator());
        sb.append(componentStatistics.getName()).append(System.lineSeparator());
        sb.append("---------------------------").append(System.lineSeparator());
        if (reportElements.isCountEnabled()) {
            sb.append(keyValue("Count", componentStatistics.getDescriptiveStatistics().getN()));
        }
        if (reportElements.isMeanEnabled()) {
            sb.append(keyValue("Mean", componentStatistics.getDescriptiveStatistics().getMean()));
        }
        if (reportElements.isStDevEnabled()) {
            sb.append(keyValue("Stdev", componentStatistics.getDescriptiveStatistics().getStandardDeviation()));
        }
        if (reportElements.isMinEnabled()) {
            sb.append(keyValue("Min", componentStatistics.getDescriptiveStatistics().getMin()));
        }
        if (reportElements.isMaxEnabled()) {
            sb.append(keyValue("Max", componentStatistics.getDescriptiveStatistics().getMax()));
        }
        if (reportElements.isPercentilesEnabled()) {
            sb.append("Percentiles").append(System.lineSeparator());
            componentStatistics.getPercentilesFor(reportElements.getPercentiles())
                    .forEach((k, v) -> sb.append(keyValue(String.valueOf(k), v)));
        }
        if (reportElements.isDistributionEnabled()) {
            sb.append("Percentage Distribution").append(System.lineSeparator());
            componentStatistics.getPercentageDistributionForRange(reportElements.getRangeMax(), reportElements.getRangeStep(), reportNumberFormat.getDf())
                    .forEach((k, v) -> sb.append(keyValue(String.valueOf(k), v)));
        }
        log.info(sb.toString());
    }

    private String keyValue(String label, double value) {
        return String.format("%s%c%s%n", label, columnSeparator, reportNumberFormat.getDf().format(value));
    }


}
