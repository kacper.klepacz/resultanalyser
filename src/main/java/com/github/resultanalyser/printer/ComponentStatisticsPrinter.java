package com.github.resultanalyser.printer;

import com.github.resultanalyser.ComponentStatistics;

public interface ComponentStatisticsPrinter {
    void print(ComponentStatistics componentStatistics);
}
