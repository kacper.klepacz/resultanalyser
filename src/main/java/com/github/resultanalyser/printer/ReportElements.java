package com.github.resultanalyser.printer;

import java.util.Arrays;
import java.util.List;

public class ReportElements {
    private final boolean countEnabled;
    private final boolean meanEnabled;
    private final boolean stDevEnabled;
    private final boolean minEnabled;
    private final boolean maxEnabled;
    private final boolean percentilesEnabled;
    private final boolean distributionEnabled;
    private final List<Double> percentiles;
    private final double rangeMax;
    private final double rangeStep;


    private ReportElements(boolean countEnabled, boolean meanEnabled, boolean stDevEnabled, boolean minEnabled, boolean maxEnabled, boolean percentilesEnabled, boolean distributionEnabled, List<Double> percentiles, double rangeMax, double rangeStep) {
        this.countEnabled = countEnabled;
        this.meanEnabled = meanEnabled;
        this.stDevEnabled = stDevEnabled;
        this.minEnabled = minEnabled;
        this.maxEnabled = maxEnabled;
        this.percentilesEnabled = percentilesEnabled;
        this.distributionEnabled = distributionEnabled;
        this.percentiles = percentiles;
        this.rangeMax = rangeMax;
        this.rangeStep = rangeStep;
    }

    public boolean isCountEnabled() {
        return countEnabled;
    }

    public boolean isMeanEnabled() {
        return meanEnabled;
    }

    public boolean isStDevEnabled() {
        return stDevEnabled;
    }

    public boolean isMinEnabled() {
        return minEnabled;
    }

    public boolean isMaxEnabled() {
        return maxEnabled;
    }

    public boolean isPercentilesEnabled() {
        return percentilesEnabled;
    }

    public boolean isDistributionEnabled() {
        return distributionEnabled;
    }

    public List<Double> getPercentiles() {
        return percentiles;
    }

    public double getRangeMax() {
        return rangeMax;
    }

    public double getRangeStep() {
        return rangeStep;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {
        boolean enableCount = true;
        boolean enableMean = true;
        boolean enableStDev = true;
        boolean enableMin = true;
        boolean enableMax = true;
        boolean enablePercentiles = true;
        boolean enableDistribution = true;
        List<Double> percentiles = Arrays.asList(10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0, 95.0, 99.0, 99.9);
        double rangeMin = 0.0;
        double rangeMax = 1.5;
        double rangeStep = 0.1;

        public Builder enableCount(boolean enableCount) {
            this.enableCount = enableCount;
            return this;
        }

        public Builder enableMean(boolean enableMean) {
            this.enableMean = enableMean;
            return this;
        }

        public Builder enableStDev(boolean enableStDev) {
            this.enableStDev = enableStDev;
            return this;
        }

        public Builder enableMin(boolean enableMin) {
            this.enableMin = enableMin;
            return this;
        }

        public Builder enableMax(boolean enableMax) {
            this.enableMax = enableMax;
            return this;
        }

        public Builder enablePercentiles(boolean enablePercentiles) {
            this.enablePercentiles = enablePercentiles;
            return this;
        }

        public Builder enableDistribution(boolean enableDistribution) {
            this.enableDistribution = enableDistribution;
            return this;
        }

        public Builder setPercentilesValues(Double... values) {
            this.percentiles = Arrays.asList(values);
            return this;
        }

        public Builder setDistributionRange(double min, double max, double step) {
            this.rangeMin = min;
            this.rangeMax = max;
            this.rangeStep = step;
            return this;
        }

        public ReportElements build() {
            return new ReportElements(enableCount, enableMean, enableStDev, enableMin, enableMax, enablePercentiles, enableDistribution,
                    percentiles, rangeMax, rangeStep);
        }
    }
}
