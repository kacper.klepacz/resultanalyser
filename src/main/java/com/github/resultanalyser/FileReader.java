package com.github.resultanalyser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

class FileReader {
    private FileReader() {
        throw new AnalyserException("Do not create object of this class!");
    }

    static Stream<String> getContentStream(List<String> fileNames) {
        return fileNames.stream().flatMap(fileName -> {
            try {
                Path path = Paths.get(fileName);
                return Files.lines(path);
            } catch (IOException e) {
                return Stream.empty();
            }
        });
    }
}
